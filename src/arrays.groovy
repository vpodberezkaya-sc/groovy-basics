import java.util.stream.Stream

/*1.2 Дан неупорядоченый массив чисел.

    найти количество различных чисел;
    найти максимальное и минимальное число;
    увеличить каждое число в 2 раза если оно положительное и в три раза если оно отрицательное;
    имеется второй массив неупорядоченных чисел: определить числа, входящие и в первый и во второй массив;*/

//find the number of different numbers
int distinctNumbers(int[] array) {
    return Arrays.stream(array).distinct()
            .count()
}

int distinctNumbers_old(int[] array) {
    HashSet unique = new HashSet()
    array.each { unique.add(it) }
    return unique.size()
}

//find max and min numbers
Map maxMin(int[] array) {
    int max = array[0]
    int min
    if (max < array[1]) {
        min = max
        max = array[1]
    } else {
        min = array[1]
    }
    for (int i = 2; i < array.size(); i++) {
        if (min > array[i]) {
            min = array[i]
        }
        if (max < array[i]) {
            max = array[i]
        }
    }
    return ['min': min, 'max': max]
}

//multiply each positive number by 2 and negative number by 3
int[] multiply(int[] array) {
    def resultArray = new int[array.length]
    for (int i = 0; i < array.length; i++) {
        if (array[i] < 0) {
            resultArray[i] = array[i] * 3
        } else {
            resultArray[i] = array[i] * 2
        }
    }
    return resultArray
}

//intersection
List intersection(int[] longArray, int[] shortArray) {
    List result = new ArrayList()
    int firstLen = longArray.length
    int secondLen = shortArray.length
    int i = 0
    int j = 0
    while (j < secondLen && i < firstLen) {
        while (longArray[i] < shortArray[j] && i < firstLen) {
            i++
        }
        if (longArray[i] == shortArray[j]) {
            result.add(longArray[i])
            j++
            i++
        } else if (longArray[i] > shortArray[j]) {
            j++
        }

    }
    return result
}

Object prepareArray(int[] first, int[] second) {
    int[] firstA = Arrays.copyOf(first, first.length)
    Arrays.sort(firstA)
    int[] secondA = Arrays.copyOf(second, second.length)
    Arrays.sort(secondA)
    if (firstA.length < secondA.length) {
        intersection(secondA, firstA)
    } else {
        intersection(firstA, secondA)
    }
}


int[] arr = [7, 1, 2, 3, 4, -4, 2, 0]
int[] second = [7, 8, 2, 6, 4]
println(arr)
println(prepareArray(arr, second))
