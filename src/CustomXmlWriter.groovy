import groovy.xml.MarkupBuilder


class CustomXmlWriter {
    private MarkupBuilder builder
    private root
    private writer = new StringWriter()
    private int invoked

    CustomXmlWriter() {
        builder = new MarkupBuilder(writer)
        invoked = 0
    }

    void setRoot(String root) {
        this.root = root
        this.builder.createNode(root)
    }

    void writeFile(String name, size_) {
        if(!invoked){
            setRoot('files')
            invoked++
        }
        this.builder.file() {
            filename("${name}")
            size("${size_}")

        }
    }


    void finish() {
        this.builder.nodeCompleted(root, root)
    }

    void writeNumber(int number) {
        this.builder."total number"("${number}")
    }

    void print() {
        this.builder.nodeCompleted(root, root)
        println(writer.toString())
    }
}

