//println('1. find all  words that contain only lower case letters');
void task1(String line) {
    String[] words = line.split("[,\\s ?!#%^&*()-]+")
    for (String word in words) {
        if (word ==~ /[a-z]+/) {
            println('matches: ' + word)
        }
    }
}
//println('2. display the range of symbols from 11th to 16th')
void task2(String line) {
    if (line.length() < 16) {
        println("The length of the provided line is ${line.length()}, but it must be > 16")
    }
    println(line.charAt(17))
    def sub = line[11..17]
    println(sub)
}

//print('3. display all English letters in lower case')//with repetitions?
void task3(String line) {
    for (int i = 0; i < line.length(); i++) {
        if (line.charAt(i) ==~ /[a-z]/) {
            println(line.charAt(i))
        }
    }
}

//without repetitions?
void task3_1(String line) {
    HashSet characters = new HashSet<>();
    for (int i = 0; i < line.length(); i++) {
        if (line.charAt(i) ==~ /[a-z]/) {
            characters.add(line.charAt(i))
        }
    }
    println(characters)
}

//println('4. set the first letter to upper case for all words')
void task4(String line) {
    def chars = line.toCharArray()
    chars[0] = Character.toUpperCase(chars[0])
    for (i = 1; i < chars.size(); i++) {
        if (chars[i] ==~ /[a-z]/ && chars[i - 1] ==~ "[\\s\\W\\d_]") {
            chars[i] = Character.toUpperCase(chars[i])
        }
    }
    println(chars)
}

//println('5. reversion')
List task5(String line) {
    List result = new ArrayList()

    def words = line.split("[,\\s ?!#%^&*()-]+")
    words.each {
        if (it && it.equalsIgnoreCase(it.reverse()) && it.chars()
                .distinct()
                .count() <5) {
            result.add(it)
        }
    }
    return result
}


//println('6. figure out whether the line contains an e-mail')
List task6(String line) {
    List result = new ArrayList()
    def words = line.split("[,\\s]+")
    words.each {
        if (it ==~ "^([a-zA-Z0-9_\\-\\.]+)@([a-zA-Z0-9_\\-.]+)\\.([a-zA-Z]{2,5})\$") {
            result.add(it)
        }
    }
    return result
}

String line = ' bob Mom _not ,obvious lordpdrol olo@mail.ru'
println(task6(line))
println(line)
//task3_1(line)