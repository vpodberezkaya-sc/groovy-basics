/**
 * Created by podberezkaya on 10/26/2018.
 */
class Person {
    String firstName
    String surName
    String address
    int age

    String toString(){
         "${firstName}, ${surName}"
    }

    def "+" (Person person){

    }
}

/*Написать класс Person (с некоторым набором свойств, к примеру firstName, surName, address (city, street, index) , age);

    Определить для объектов класса методы '+', '-';
    Определить метод toString, который выводит "${firstName}, ${surName}"
    Среди объектов типа Person найти такие, у которых возраст (age) менее 30 лет;
    Вывести все различные адреса, которые есть у списка объектов Person;
    Переопределить метод toString, не меняя описания класса Person (к примеру теперь пусть выводит "${surName} ${firstName} (${age})").
*/