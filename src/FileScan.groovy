import static groovy.io.FileType.FILES


void findMatches(String pathname, String pattern, output) {
    int count = 0
    new File(pathname).eachFileRecurse(FILES) {
        if (it.name ==~ pattern) {
            count++
            output.writeFile(it.name, it.size() / 1000)
        }
    }
    output.writeNumber(count)
    output.print()
}


class consoleOutput {
    void writeFile(String name, size_) {
        println('FileName: ' + name)
        println("Size (in kb): " + size_)
    }

    void writeNumber(count) {
        println('Quantity of Files: ' + count)
    }

    void print() {}
}
//-----------------------------
String pattern = ".*[^.]groovy.*"
findMatches("C:\\Users\\podberezkaya\\Desktop\\groovy-basics", pattern, new CustomXmlWriter())









